import java.util.Comparator;

public class PersonNameComparator implements Comparator<IPerson> {

    @Override
    public int compare(IPerson o1, IPerson o2) {

        if (o1.lastName().equals(o2.lastName()))
            return o1.firstName().compareTo(o2.firstName());

        return o1.lastName().compareTo(o2.lastName());
    }
}
