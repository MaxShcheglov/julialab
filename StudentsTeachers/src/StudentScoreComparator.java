import java.util.Comparator;

public class StudentScoreComparator implements Comparator<Student> {

    @Override
    public int compare(Student a, Student b) {
        return a.score() < b.score() ? -1 : a.score() == b.score() ? 0 : 1;
    }
}
