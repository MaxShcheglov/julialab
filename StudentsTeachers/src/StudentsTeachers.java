import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class StudentsTeachers {
    public static void main(String[] args) {

        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student("Rishi", "Berry", 5.2));
        students.add(new Student("Lillian", "Melton", 2.2));
        students.add(new Student("Sidney", "Higgins", 3.5));
        students.add(new Student("Kathy", "Rangel", 6.3));
        students.add(new Student("Albus", "Rangel", 6.2));

        ArrayList<Teacher> teachers = new ArrayList<>();

        teachers.add(new Teacher("Danielle", "Roman"));
        teachers.add(new Teacher("Damaris", "Le"));
        teachers.add(new Teacher("Landyn", "Gonzales"));
        teachers.add(new Teacher("Alexa", "Dominguez"));
        teachers.add(new Teacher("Kathy", "Rangel")); // this is name repetition

        ArrayList<IPerson> persons = new ArrayList<IPerson>();
        persons.addAll(students);
        persons.addAll(teachers);

        ShowPersons(persons);
        ShowPersonsNamesWithoutRepetition(persons);
        ShowStudentsSortedWithAverageScore(students);
        ShowStudentsSortedWithLastNameAndFirstName(students);
        ShowPersonsSortedWithLastNameAndFirstName(persons);
    }

    private static void ShowPersons(ArrayList<IPerson> persons) {
        System.out.println("persons: ");
        for (IPerson person : persons) {
            System.out.println(person);
        }
    }

    private static void ShowPersonsNamesWithoutRepetition(ArrayList<IPerson> persons) {

        ArrayList<IPerson> personsWithoutRepetition = new ArrayList<IPerson>();

        for (IPerson person : persons) {

            boolean isPersonNameUnique = true;

            for (IPerson uniquePerson : personsWithoutRepetition) {
                if (uniquePerson.firstName().equals(person.firstName())) {
                    isPersonNameUnique = false;
                    break;
                }
            }

            if (isPersonNameUnique)
                personsWithoutRepetition.add(person);
        }

        System.out.println("\npersons names without repetition: ");
        for (IPerson person : personsWithoutRepetition) {
            System.out.println(person.firstName());
        }
    }

    private static void ShowStudentsSortedWithAverageScore(ArrayList<Student> students) {

        Collections.sort(students, new StudentScoreComparator());

        System.out.println("\nstudents sorted with average score: ");
        for (IPerson person : students) {
            System.out.println(person);
        }
    }

    private static void ShowStudentsSortedWithLastNameAndFirstName(ArrayList<Student> students) {

        Collections.sort(students, new StudentNameComparator());

        System.out.println("\nstudents sorted with last name and first name: ");
        for (IPerson person : students) {
            System.out.println(person);
        }
    }

    private static void ShowPersonsSortedWithLastNameAndFirstName(ArrayList<IPerson> persons) {

        Collections.sort(persons, new PersonNameComparator());

        System.out.println("\npersons sorted with last name and first name: ");
        for (IPerson person : persons) {
            System.out.println(person);
        }
    }
}

