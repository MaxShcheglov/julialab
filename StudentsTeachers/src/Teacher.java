public class Teacher implements IPerson {
    private String firstName;
    private String lastName;

    Teacher(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String firstName() {
        return firstName;
    }

    public String lastName() {
        return lastName;
    }

    @Override
    public Boolean isTeacher() {
        return true;
    }

    @Override
    public String toString() {
        return lastName + ", " + firstName + " [teacher]";
    }
}
