import java.util.Comparator;

public class StudentNameComparator implements Comparator<Student> {

    @Override
    public int compare(Student one, Student other) {

        if (one.lastName().equals(other.lastName()))
            return one.firstName().compareTo(other.firstName());

        return one.lastName().compareTo(other.lastName());
    }
}
