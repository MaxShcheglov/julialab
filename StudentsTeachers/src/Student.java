public class Student implements IPerson {
    private String firstName;
    private String lastName;
    private double score;

    Student(String firstName, String lastName, Double score) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.score = score;
    }

    public String firstName() {
        return firstName;
    }

    public String lastName() {
        return lastName;
    }

    @Override
    public Boolean isTeacher() {
        return false;
    }

    public Double score() {
        return score;
    }

    @Override
    public String toString() {
        return lastName + ", " + firstName + " [" + score + "]";
    }
}
