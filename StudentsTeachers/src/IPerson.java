public interface IPerson {
    String firstName();
    String lastName();
    Boolean isTeacher();
}
