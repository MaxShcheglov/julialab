import java.util.NoSuchElementException;

public class CustomQueue {
    private int front;
    private int rear;
    private int capacity;
    private int[] array;


    CustomQueue(int capacity) {
        this.capacity = capacity;
        front = rear = 0;
        array = new int[capacity];
    }

    public void add(int element) {
        if (capacity == rear) {
            throw new IllegalStateException("queue is full");
        }

        array[rear] = element;
        rear++;
    }

    public int poll() {
        if (front == rear) {
            throw new NoSuchElementException("queue is empty");
        }

        int polledElement = array[front];

        for (int i = 0; i < rear - 1; i++) {
            array[i] = array[i + 1];
        }

        if (rear < capacity)
            array[rear] = 0;

        rear--;

        return polledElement;
    }

    public void display(String message) {
        System.out.printf("%s: ", message);

        for (int i = front; i < rear; i++) {
            System.out.printf(" %d <-- ", array[i]);
        }
        System.out.print("\n");
    }
}
