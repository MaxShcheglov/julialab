import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class QueueDemonstration {
    public static void main(String[] args) {

        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);

        if (args.length > 0) {
            if (args[0].equals("commandline")) {

                // commandline add add add add add poll poll poll add poll
                String[] commands = Arrays.copyOfRange(args, 1, args.length);
                System.out.println("passing parameters through the command line arguments: " + String.join(" ", commands));
                ProcessCommands(queue, commands);

            } else if (args[0].equals("random")) {

                // random
                String[] commands = GetRandomCommands(10);
                System.out.println("passing parameters through the random string generator: " + String.join(" ", commands));
                ProcessCommands(queue, commands);

            }
        } else {
            ProcessManualInput(queue);
        }
    }

    private static String[] GetRandomCommands(int numberOfCommands) {
        String[] array = new String[] { "add", "poll" };
        String[] commands = new String [numberOfCommands];
        for (int i = 0; i < numberOfCommands; i++) {
            Random randomIndex = new Random();
            commands[i]=array[randomIndex.nextInt(array.length)];
        }
        return commands;
    }

    private static void ProcessCommands(Queue<Integer> queue, String[] args) {
        for (int i = 0; i < args.length; i++) {
            String command = args[i];

            if (command.equals("add")) {
                int randomValue = new Random().nextInt(99);
                queue.add(randomValue);
            } else if (command.equals("poll")) {
                queue.poll();
            }

            System.out.println("queue after command [" + command + "]: " + queue);
        }
    }

    private static void ProcessManualInput(Queue<Integer> queue) {

        queue.add(4);
        queue.add(6);
        queue.add(7);
        queue.add(8);
        queue.add(2);
        queue.add(90);
        queue.add(7);

        System.out.println("queue: " + queue);

        System.out.println("element polled: " + queue.poll());
        System.out.println("element polled: " + queue.poll());
        System.out.println("element polled: " + queue.poll());

        System.out.println("queue after 3 elements polled: " + queue);
    }
}
