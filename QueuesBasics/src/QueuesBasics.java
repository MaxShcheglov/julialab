import java.util.LinkedList;
import java.util.Queue;

public class QueuesBasics {
    public static void main(String[] args) {

        customQueueDemonstration();

        systemQueueDemonstration();
    }

    private static void customQueueDemonstration() {
        System.out.println("\n\tcustom queue demonstration: \n");

        CustomQueue queue = new CustomQueue(5);

        queue.add(10);
        queue.add(20);
        queue.add(30);
        queue.add(40);
        queue.add(50);

        try {
            System.out.print("try to add 60... ");
            queue.add(60);
        } catch(Exception e) {
            System.out.println(e);
        }

        queue.display("source queue");
        queue.display("polled element " + queue.poll());
        queue.add(50);
        queue.display("add element " + 50);
        queue.display("after dequeue element " + queue.poll());
        queue.display("after dequeue element " + queue.poll());
        queue.display("after dequeue element " + queue.poll());
        queue.display("after dequeue element " + queue.poll());
        queue.display("after dequeue element " + queue.poll());

        try {
            System.out.print("try to peek... ");
            queue.poll();
        } catch(Exception e) {
            System.out.println(e);
        }
    }

    private static void systemQueueDemonstration() {
        System.out.println("\n\tsystem queue demonstration: \n");

        Queue<Integer> queue = new LinkedList<Integer>();

        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);

        System.out.println("elements in queue: " + queue);
        System.out.println("removed element: " + queue.remove());
        System.out.println("elements in queue: " + queue);
        System.out.println("head of queue: " + queue.peek());
        System.out.println("Size of queue: " + queue.size());
    }
}

