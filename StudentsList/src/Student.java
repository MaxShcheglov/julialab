public class Student{
    private String name;
    private double score;

    public Student(String name, double score) {
        this.name = name;
        this.score = score;
    }

    public String Name() {
        return name;
    }

    public double Score() {
        return score;
    }
}
