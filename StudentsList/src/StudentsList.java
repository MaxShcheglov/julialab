import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StudentsList {
    public static void main(String[] args) {

        List<Student> students = Arrays.asList(
                new Student("Dinklage", 6.8),
                new Student("Hawkins", 4.6),
                new Student("Jackson", 1.2),
                new Student("Moore", 2.0),
                new Student("Peterson", 5.1)
        );

        displayStudents("name sorted students", students);
        Collections.sort(students, (a, b) -> a.Score() < b.Score() ? -1 : a.Score() == b.Score() ? 0 : 1);
        displayStudents("score sorted students", students);
    }

    private static void displayStudents(String message, List<Student> students) {
        System.out.println(message + ":");

        for (Student student : students) {
            System.out.println(student.Name() + "[" + student.Score() + "]");
        }

        System.out.println();
    }
}

