public class CustomStackDemonstration {

    public static void main(String[] args) {
        CustomStack stack = new CustomStack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        try {
            while (true) {
                stack.push(25);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        ShowStack(stack, "Using custom stack:");

        int peekedElement = stack.peek();
        System.out.println("peeked element: " + peekedElement);

        int poppedElement = stack.pop();
        System.out.println("Popped element: " + poppedElement);

        poppedElement = stack.pop();
        System.out.println("Popped element: " + poppedElement);

        try {
            while (true) {
                poppedElement = stack.pop();
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            while (true) {
                peekedElement = stack.peek();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void ShowStack(CustomStack stack, String message) {
        StringBuilder builder = new StringBuilder();
        builder.append(message);
        for (int i = 0; i < stack.InternalArray().length; i++) {
            builder.append(String.format(" %d", stack.InternalArray()[i]));
        }
        builder.append("\n");
        System.out.println(builder.toString());
    }
}
