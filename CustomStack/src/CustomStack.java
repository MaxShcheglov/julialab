
public class CustomStack {
	private int[] array;
	private int index;
    	
	public CustomStack() {
		array = new int[10];
		index = -1;
	}
    
	public CustomStack(int capacity) {
		array = new int[capacity];
		index = -1;
	}

	public int pop() {
		if (isEmpty())
			throw new RuntimeException("can't pop, stack is empty");

		return array[index--];
	}
	
	public int peek() {
		if (isEmpty())
			throw new RuntimeException("can't peek, stack is empty");
		
		return array[index];
	}

	public void push(int element) {
		if (isFull())
			throw new RuntimeException("can't push, stack is full");
		
		array[++index] = element;
	}

	public boolean isEmpty() {
		return index == -1;
	}
	
	public boolean isFull() {
		return index == array.length - 1;
	}
	
	public int[] InternalArray() {
		return array;
	}
}