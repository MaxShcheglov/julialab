import java.util.Arrays;
import java.util.Random;
import java.util.Stack;

public class StackDemonstration {
    public static void main(String[] args) {

        Stack<Integer> stack = new Stack<Integer>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        if (args.length > 0) {
            if (args[0].equals("commandline")) {

                // commandline push push push push pop pop push pop push pop pop
                String[] commands = Arrays.copyOfRange(args, 1, args.length);
                System.out.println("passing parameters through the command line arguments: " + String.join(" ", commands));
                ProcessCommands(stack, commands);

            } else if (args[0].equals("random")) {

                // random
                String[] commands = GetRandomCommands(10);
                System.out.println("passing parameters through the random string generator: " + String.join(" ", commands));
                ProcessCommands(stack, commands);

            }
        } else {
            ProcessManualInput(stack);
        }
    }

    private static String[] GetRandomCommands(int numberOfCommands) {
        String[] array = new String[] { "push", "pop" };
        String[] commands = new String [numberOfCommands];
        for (int i = 0; i < numberOfCommands; i++) {
            Random randomIndex = new Random();
            commands[i]=array[randomIndex.nextInt(array.length)];
        }
        return commands;
    }

    private static void ProcessCommands(Stack<Integer> stack, String[] args) {
        for (int i = 0; i < args.length; i++) {
            String command = args[i];

            if (command.equals("push")) {
                int randomValue = new Random().nextInt(99);
                stack.push(randomValue);
            } else if (command.equals("pop")) {
                stack.pop();
            }

            System.out.println("stack after command [" + command + "]: " + stack);
        }
    }

    private static void ProcessManualInput(Stack<Integer> stack) {

        stack.push(4);
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(2);
        stack.push(90);
        stack.push(7);

        System.out.println("stack: " + stack);

        System.out.println("element popped: " + stack.pop());
        System.out.println("element popped: " + stack.pop());
        System.out.println("element popped: " + stack.pop());

        System.out.println("stack after 3 elements popped: " + stack);
    }
}
