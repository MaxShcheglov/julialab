public class Main {
    public static void main(String[] args) {
        UnsecureStack stack = new UnsecureStack();

        stack.push("first element");
        stack.push("second element");

        // wrong access modifier, you cant set index field directly
        stack.index = 99;

        try {
            stack.peek();
        } catch(Exception e) {
            System.out.println(e);
        }

        // wrong access modifier, you cant set array field directly
        stack.array = null;

        try {
            stack.push("third element");
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
