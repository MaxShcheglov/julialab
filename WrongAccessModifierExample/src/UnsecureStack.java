public class UnsecureStack {
    public String[] array;
    public int index;

    public UnsecureStack() {
        array = new String[10];
        index = -1;
    }

    public void push(String element) {
        array[++index] = element;
    }

    public String pop() {
        return array[index--];
    }

    public String peek() {
        return array[index];
    }
}
