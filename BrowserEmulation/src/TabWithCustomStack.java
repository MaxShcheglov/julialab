public class TabWithCustomStack {
    private CustomStack historyStack = new CustomStack();
    private CustomStack forwardStack = new CustomStack();
    private String currentPage = "";

    public void forward() {
        if (forwardStack.isEmpty()) {
            Log("cant go forward, forward stack is empty");
            return;
        }

        historyStack.push(currentPage);
        currentPage = forwardStack.pop();

        Log("forward to " + currentPage);
    }

    public void back() {
        if (historyStack.isEmpty()) {
            Log("cant go back, history is empty");
            return;
        }

        forwardStack.push(currentPage);
        currentPage = historyStack.pop();

        Log("back to " + currentPage);
    }

    public void navigate(String page) {
        if (currentPage != "")
            historyStack.push(currentPage);

        forwardStack.empty();
        currentPage = page;

        Log("navigate to " + currentPage);
    }

    private void Log(String message) {
        System.out.println(message);
    }
}
