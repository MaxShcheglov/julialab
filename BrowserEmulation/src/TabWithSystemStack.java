import java.util.Stack;

public class TabWithSystemStack {
    private Stack<String> historyStack = new Stack();
    private Stack<String> forwardStack = new Stack();
    private String currentPage = "";

    public void forward() {
        if (forwardStack.isEmpty()) {
            Log("cant go forward, forward stack is empty");
            return;
        }

        historyStack.add(currentPage);
        currentPage = forwardStack.pop();

        Log("forward to " + currentPage);
    }

    public void back() {
        if (historyStack.isEmpty()) {
            Log("cant go back, history is empty");
            return;
        }

        forwardStack.add(currentPage);
        currentPage = historyStack.pop();

        Log("back to " + currentPage);
    }

    public void navigate(String page) {
        if (currentPage != "")
            historyStack.add(currentPage);

        forwardStack.empty();
        currentPage = page;

        Log("navigate to " + currentPage);
    }

    private void Log(String message) {
        System.out.println(message);
    }
}
