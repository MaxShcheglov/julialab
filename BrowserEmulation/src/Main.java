import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //TabWithSystemStack tab = new TabWithSystemStack();
        TabWithCustomStack tab = new TabWithCustomStack();

        if (args.length > 0) {
            if (args[0].equals("random")) {

                // random
                String[] commands = GetRandomCommands(10);
                System.out.println("passing parameters through the random string generator: " + String.join(" ", commands));
                ProcessCommands(tab, commands);

            } else if (args[0].equals("commandline")) {

                // commandline google.com back vk.com forward twitter.com odnoklassniki.ru mail.ru forward back back
                String[] commands = Arrays.copyOfRange(args, 1, args.length);
                System.out.println("passing parameters through the command line arguments: " + String.join(" ", commands));
                ProcessCommands(tab, commands);

            } else if (args[0].equals("scanner")) {

                // scanner
                String[] commands = GetCommandsWithScanner("vk.com\nback\ngoogle.com\ntwitter.com\nback\nforward\nforward");
                System.out.println("passing parameters through the scanner: " + String.join(" ", commands));
                ProcessCommands(tab, commands);

            } else if (args[0].equals("manual")) {

                // manual
                System.out.println("passing parameters through the manual input...");
                ProcessManualInput(tab);

            } else {

                // wrong arguments passed
                System.out.println("wrong parameters was passed, use random, commandline, scanner or manual");

            }
        } else {
            System.out.println("no parameters was passed, there is no commands to execute...");
        }
    }

    // manual input command processing
    private static void ProcessManualInput(TabWithCustomStack tab) {
        tab.navigate("vk.com");
        tab.navigate("google.com");
        tab.navigate("facebook.com");
        tab.back();
        tab.back();
        tab.forward();
        tab.forward();
        tab.back();
        tab.back();
        tab.navigate("twitter.com");
        tab.back();
        tab.back();
        tab.forward();
    }

    // user input command processing
    private static void ProcessCommands(TabWithCustomStack tab, String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("back")) {
                tab.back();
            } else if (args[i].equals("forward")) {
                tab.forward();
            } else {
                tab.navigate(args[i]);
            }
        }
    }

    // random command generator
    private static String[] GetRandomCommands(int numberOfCommands) {
        String[] array = new String[] { "vk.com", "google.com", "twitter.com", "back", "forward" };
        String[] commands = new String [numberOfCommands];
        for (int i = 0; i < numberOfCommands; i++) {
            Random randomIndex = new Random();
            commands[i]=array[randomIndex.nextInt(array.length)];
        }
        return commands;
    }

    // commands from scanner
    private static String[] GetCommandsWithScanner(String input) {
        Scanner scanner = new Scanner(input);
        int numberOfcommands = input.split("\n").length;
        String[] commands = new String[numberOfcommands];
        for (int i = 0; i < commands.length; i++) {
            commands[i] = scanner.nextLine();
        }
        return commands;
    }
}
