public class CustomStack {
    private String[] array;
    private int index;

    public CustomStack() {
        array = new String[10];
        index = -1;
    }

    public CustomStack(int capacity) {
        array = new String[capacity];
        index = -1;
    }

    public String pop() {
        if (isEmpty())
            throw new RuntimeException("can't pop, stack is empty");

        return array[index--];
    }

    public String peek() {
        if (isEmpty())
            throw new RuntimeException("can't peek, stack is empty");

        return array[index];
    }

    public void push(String element) {
        if (isFull())
            throw new RuntimeException("can't push, stack is full");

        array[++index] = element;
    }
    public void empty() {
        array = new String[10];
        index = -1;
    }

    public boolean isEmpty() {
        return index == -1;
    }

    public boolean isFull() {
        return index == array.length - 1;
    }

    public String[] InternalArray() {
        return array;
    }
}
