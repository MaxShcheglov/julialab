public class IntegerStack extends IntegerServiceClass {
    public IntegerStack(int capacity) {
        super(capacity);
    }

    public void push(int element) {
        super.push(element);
    }

    public int pop() {
        return super.pop();
    }

    @Override
    public String toString() {
        String output = "[";
        for (int i = 0; i <= index; i++) {
            if (array[i] > 0) {
                output += array[i] + ", ";
            }
        }
        output = output.substring(0, output.length() - 2);
        output += "]";
        return output;
    }
}

