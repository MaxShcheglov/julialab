public class IntegerQueue extends IntegerServiceClass {
    public IntegerQueue(int capacity) {
        super(capacity);
    }

    public void add (int element) {
        super.add(element);
    }

    public int poll() {
        return super.poll();
    }

    @Override
    public String toString() {
        String output = "[";
        for (int i = front; i < rear; i++) {
            if (array[i] > 0) {
                output += array[i] + ", ";
            }
        }
        output = output.substring(0, output.length() - 2);
        output += "]";
        return output;
    }
}