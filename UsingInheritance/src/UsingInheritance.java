public class UsingInheritance {
    public static void main(String[] args) {
        IntegerStackDemonstration();
        IntegerQueueDemonstration();
        ObjectStackDemonstration();
        ObjectQueueDemonstration();
    }

    private static void IntegerStackDemonstration() {
        IntegerStack stack = new IntegerStack(10);

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        System.out.println("\nstack: " + stack);
        System.out.println("pop " + stack.pop() + ": " + stack);
        System.out.println("pop " + stack.pop() + ": " + stack);
        System.out.println("pop " + stack.pop() + ": " + stack);
        System.out.println("pop " + stack.pop() + ": " + stack);

        stack.push(77);
        System.out.println("pushed 77: " + stack);
        stack.push(56);
        System.out.println("pushed 56: " + stack);
        stack.push(43);
        System.out.println("pushed 43: " + stack);

        System.out.println("stack: " + stack);
    }

    private static void IntegerQueueDemonstration() {
        IntegerQueue queue = new IntegerQueue(10);

        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);

        System.out.println("\nqueue: " + queue);
        System.out.println("poll " + queue.poll() + ": " + queue);
        System.out.println("poll " + queue.poll() + ": " + queue);
        System.out.println("poll " + queue.poll() + ": " + queue);
        System.out.println("poll " + queue.poll() + ": " + queue);

        queue.add(77);
        System.out.println("add 77: " + queue);
        queue.add(56);
        System.out.println("add 56 " + queue);
        queue.add(43);
        System.out.println("add 43 " + queue);

        System.out.println("queue: " + queue);
    }

    private static void ObjectStackDemonstration() {
        ObjectStack stack = new ObjectStack(5);

        stack.push(new Student("john"));
        stack.push(new Student("michael"));
        stack.push(new Student("carl"));
        stack.push(new Student("peter"));
        stack.push(new Student("homer"));

        System.out.println("\nstack: " + stack);
        System.out.println("pop " + stack.pop() + ": " + stack);
        System.out.println("pop " + stack.pop() + ": " + stack);
        System.out.println("pop " + stack.pop() + ": " + stack);
        System.out.println("pop " + stack.pop() + ": " + stack);

        stack.push(77);
        System.out.println("pushed 77: " + stack);
        stack.push(56);
        System.out.println("pushed 56: " + stack);
        stack.push(43);
        System.out.println("pushed 43: " + stack);

        System.out.println("stack: " + stack);
    }

    private static void ObjectQueueDemonstration() {
        ObjectQueue queue = new ObjectQueue(5);

        queue.add(new Student("john"));
        queue.add(new Student("michael"));
        queue.add(new Student("carl"));
        queue.add(new Student("peter"));
        queue.add(new Student("homer"));

        System.out.println("\nqueue: " + queue);
        System.out.println("poll " + queue.poll() + ": " + queue);
        System.out.println("poll " + queue.poll() + ": " + queue);
        System.out.println("poll " + queue.poll() + ": " + queue);
        System.out.println("poll " + queue.poll() + ": " + queue);

        queue.add(77);
        System.out.println("add 77: " + queue);
        queue.add(56);
        System.out.println("add 56: " + queue);
        queue.add(43);
        System.out.println("add 43: " + queue);

        System.out.println("queue: " + queue);
    }
}

