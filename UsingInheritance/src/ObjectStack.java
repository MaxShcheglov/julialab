public class ObjectStack extends ObjectServiceClass {
    public ObjectStack(int capacity) {
        super(capacity);
    }

    public void push(int element) {
        super.push(element);
    }

    public Object pop() {
        return super.pop();
    }

    @Override
    public String toString() {
        String output = "[";
        for (int i = 0; i <= index; i++) {
            if (array[i] != null) {
                output += array[i] + ", ";
            }
        }
        output = output.substring(0, output.length() - 2);
        output += "]";
        return output;
    }
}