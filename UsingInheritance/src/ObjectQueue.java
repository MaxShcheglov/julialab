public class ObjectQueue extends ObjectServiceClass {
    public ObjectQueue(int capacity) {
        super(capacity);
    }

    public void add (Object element) {
        super.add(element);
    }

    public Object poll() {
        return super.poll();
    }

    @Override
    public String toString() {
        String output = "[";
        for (int i = front; i < rear; i++) {
            if (array[i] != null) {
                output += array[i] + ", ";
            }
        }
        output = output.substring(0, output.length() - 2);
        output += "]";
        return output;
    }
}