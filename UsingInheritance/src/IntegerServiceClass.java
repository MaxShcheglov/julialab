public class IntegerServiceClass {
    protected int[] array;
    protected int index;
    protected int front;
    protected int rear;
    protected int capacity;

    public IntegerServiceClass(int capacity) {
        this.capacity = capacity;
        array = new int[capacity];
        front = rear = 0;
        index = -1;
    }

    protected void add(int element) {
        array[rear] = element;
        rear++;
    }

    protected int poll() {
        int polledElement = array[front];
        ShiftElementOnTheLeft();
        rear--;
        return polledElement;
    }

    private void ShiftElementOnTheLeft() {
        for (int i = 0; i < rear - 1; i++) {
            array[i] = array[i + 1];
        }

        if (rear < capacity)
            array[rear] = 0;
    }

    protected void push(int element) {
        array[++index] = element;
    }

    protected int pop() {
        return array[index--];
    }
}
