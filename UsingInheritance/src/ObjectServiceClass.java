public class ObjectServiceClass {
    protected Object[] array;
    protected int index;
    protected int front;
    protected int rear;
    protected int capacity;

    public ObjectServiceClass(int capacity) {
        this.capacity = capacity;
        array = new Object[capacity];
        front = rear = 0;
        index = -1;
    }

    protected void add(Object element) {
        array[rear] = element;
        rear++;
    }

    protected Object poll() {
        Object polledElement = array[front];
        ShiftElementOnTheLeft();
        rear--;
        return polledElement;
    }

    private void ShiftElementOnTheLeft() {
        for (int i = 0; i < rear - 1; i++) {
            array[i] = array[i + 1];
        }

        if (rear < capacity)
            array[rear] = 0;
    }

    protected void push(Object element) {
        array[++index] = element;
    }

    protected Object pop() {
        return array[index--];
    }
}
