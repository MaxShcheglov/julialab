
public class ColumnSortWithArgsAndModuleRestriction {

	public void Run(String[] args) {
		
		Boolean useDefault = args.length != 4;
		
		int rowsCount = useDefault ? 4 : Integer.parseInt(args[0]);
		int columnsCount = useDefault ? 4 : Integer.parseInt(args[1]);
		String sortingType = useDefault ? "asc" : args[2];
		int moduleRestriction = useDefault ? 10 : Integer.parseInt(args[3]);
		
	    int [][] twoDimensionalArray = CreateTwoDimensionalArray(rowsCount, columnsCount);

	    InitializeTwoDimensionalArrayWithValues(twoDimensionalArray, moduleRestriction);

        ShowArrayWithMessage(twoDimensionalArray, "\nSource array");
	    
        twoDimensionalArray = TransposeTwoDimensionalArray(twoDimensionalArray);
	    
		long startTime = System.currentTimeMillis();
	    
    	switch (sortingType) {
	    	case "asc":
	    		
	    		twoDimensionalArray = SortArrayByColumnAscending(twoDimensionalArray);
	    		twoDimensionalArray = TransposeTwoDimensionalArray(twoDimensionalArray);
	    		ShowArrayWithMessage(twoDimensionalArray, "\nModified array");
	    		
	    		break;
	    	case "desc":
	    		
	    		twoDimensionalArray = SortArrayByColumnDescending(twoDimensionalArray);
	    		twoDimensionalArray = TransposeTwoDimensionalArray(twoDimensionalArray);
	    		ShowArrayWithMessage(twoDimensionalArray, "\nModified array");
	    		
	    		break;
	    	case "both":
	    		
	    		twoDimensionalArray = SortArrayByColumnAscending(twoDimensionalArray);
	    		twoDimensionalArray = TransposeTwoDimensionalArray(twoDimensionalArray);
	    		ShowArrayWithMessage(twoDimensionalArray, "\nModified array");
	    	    
	            twoDimensionalArray = TransposeTwoDimensionalArray(twoDimensionalArray);
	    		
	    		twoDimensionalArray = SortArrayByColumnDescending(twoDimensionalArray);
	    		twoDimensionalArray = TransposeTwoDimensionalArray(twoDimensionalArray);
	    		ShowArrayWithMessage(twoDimensionalArray, "\nModified array");
	    		
	    		break;
    	}
	    
	    long estimatedTime = System.currentTimeMillis() - startTime;
	    
		System.out.println("\nSorting time: " + estimatedTime);
	}
	
	private int[] SortArrayDescending(int[] array) {
				
		int temp = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = 1; j < (array.length - i); j++) {
				if (array[j - 1] < array[j]) {
					temp = array[j - 1];
					array[j - 1] = array[j];
					array[j] = temp;
				}
			}
		}		
		
		return array;
	}
	
	private int[] SortArrayAscending(int[] array) {
				
		int temp = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = 1; j < (array.length - i); j++) {
				if (array[j - 1] > array[j]) {
					temp = array[j - 1];
					array[j - 1] = array[j];
					array[j] = temp;
				}
			}
		}		
		
		return array;
	}
	
	private void ShowArrayWithMessage(int[][] array, String message) {
		
	    System.out.println(message);
	    for (int i = 0; i < array.length; i++){
	        for (int j = 0; j < array[i].length; j++){
	            System.out.print(array[i][j] + " ");
	        }
	        System.out.println("");
	    }		
	}
	
	private int[][] CreateTwoDimensionalArray(int rowsCount, int columnsCount) {
		
	    int [][] twoDimensionalArray = new int[rowsCount][];
        for (int i = 0; i < rowsCount; i++){
        	twoDimensionalArray[i] = new int[columnsCount];
	    }
        
        return twoDimensionalArray;
	}
	
	private void InitializeTwoDimensionalArrayWithValues(int[][] twoDimensionalArray, int moduleRestriction) {
		
        for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	            twoDimensionalArray[i][j] = (int) (Math.random() * moduleRestriction + 1);
	        }
	    }
	}
	
	private int[][] TransposeTwoDimensionalArray(int[][] twoDimensionalArray) {
		
		int [][] transposedArray = CreateTwoDimensionalArray(twoDimensionalArray.length, twoDimensionalArray[0].length);
		    
	    for (int i = 0; i < twoDimensionalArray.length; i++) {
	    	for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	    		transposedArray[i][j] = twoDimensionalArray[j][i];
	    	}
	    }
		
	    return transposedArray;
	}
	
	private int[][] SortArrayByColumnDescending(int[][] array) {
		
	    for (int i = 0; i < array.length; i++) {
	    	array[i] = SortArrayDescending(array[i]);
	    }
	    
	    return array;		
	}
	
	private int[][] SortArrayByColumnAscending(int[][] array) {
		
	    for (int i = 0; i < array.length; i++) {
	    	array[i] = SortArrayAscending(array[i]);
	    }
	    
	    return array;	
	}
}
