import java.util.Stack;

public class StackPopPushes {

	public void Run() {
		
		Stack<Integer> stack = new Stack<Integer>();		
		int[] popPushQueue = new int[10];
		
        for (int i = 0; i < popPushQueue.length; i++) {
        	popPushQueue[i] = (int) (Math.random() * 2);
	    }
        
        ShowArrayWithMessage(popPushQueue, "\nPopPushQueue: ");
        
        System.out.println("\n");
        
        int poppedValue = 0;
        int pushedValue = 0;
        int popCounter = 0;
        int pushCounter = 0;
        int[] popArray = new int[10];
        int[] pushArray = new int[10];
        
        for (int i = 0; i < popPushQueue.length; i++) {
        	if (popPushQueue[i] == 1) {        		
        		pushedValue = (int) (Math.random() * 100);
        		stack.push(pushedValue);
        		pushArray[pushCounter] = pushedValue;        		
        		pushCounter++;
        		
        		System.out.println("pushed: " + pushedValue);
        	} else {      		
        		if (!stack.isEmpty()) {
        			poppedValue = stack.pop();
        			popArray[popCounter] = poppedValue;
        			popCounter++;
        			
        			System.out.println("popped: " + poppedValue);
        		} else {
        			System.out.println("can't pop, stack is empty: ");
        		}
        	}
        }
        
        ShowArrayWithMessage(pushArray, "\npushed values: ");
        ShowArrayWithMessage(popArray, "\npopped values: ");
	}
	
	private void ShowArrayWithMessage(int[] array, String message) {
		
	    System.out.println(message);
	    for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
	    }		
	}	
}
