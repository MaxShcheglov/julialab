
public class Main {

	public static void main(String[] args) {
	
//		new FindMinimumInTwoDimensionalArray().Run();
//		new FindMaximumInTwoDimensionalArray().Run();
//		new MoreThanOneMaximumMinimum().Run();
//		new MoreThanOneMaximumMinimumWithCounter().Run();		
//		new AscensionColumnSortWithArgs().Run(new String[] { "5", "5" });
//		new ColumnSortWithArgsAndModuleRestriction().Run(new String[] { "5", "5", "asc", "10"});
//		new ColumnSortWithArgsAndModuleRestriction().Run(new String[] { "5", "5", "desc", "10"});
//		new ColumnSortWithArgsAndModuleRestriction().Run(new String[] { "5", "5", "both", "10" });
//		new ColumnSortWithArgsAndModuleRestriction().Run(new String[] { "5", "5" });
//		new StackPopPushes().Run();
		new StackPopPushesWithArgs().Run(new String[] { "20", "3"});
	}	
}
