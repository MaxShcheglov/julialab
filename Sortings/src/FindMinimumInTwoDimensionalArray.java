
public class FindMinimumInTwoDimensionalArray {

	public void Run() {
		
	    int maxRows = 3;
	    int maxCols = 4;

	    int [][] twoDimensionalArray = new int[maxRows][];
	    for (int i = 0; i < maxRows; i++){
	    	twoDimensionalArray[i] = new int[maxCols];
	    }

	    for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	            twoDimensionalArray[i][j] = (int) (Math.random() * 100);
	        }
	    }

	    System.out.println("\nSource array");
	    for (int i = 0; i < twoDimensionalArray.length; i++){
	        for (int j = 0; j < twoDimensionalArray[i].length; j++){
	            System.out.print(twoDimensionalArray[i][j] + " ");
	        }
	        System.out.println("");
	    }
	    
	    int minValue = 100;
	    System.out.println("\nMin value in array: ");
	    for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	        	if (twoDimensionalArray[i][j] < minValue) {
	        		minValue = twoDimensionalArray[i][j];
	        	}
	            
	        }
	    }
	    
	    System.out.println(minValue);
	}
}
