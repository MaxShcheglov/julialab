import java.util.Arrays;

public class AscensionColumnSortWithArgs {

	public void Run(String[] args) {
		
		int rowsCount = Integer.parseInt(args[0]);
		int columnCount = Integer.parseInt(args[1]);
		
	    int [][] twoDimensionalArray = new int[rowsCount][];
        for (int i = 0; i < rowsCount; i++){
        	twoDimensionalArray[i] = new int[columnCount];
	    }
        
	    int [][] transposedArray = new int[rowsCount][];
        for (int i = 0; i < rowsCount; i++){
        	transposedArray[i] = new int[columnCount];
	    }

        for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	            twoDimensionalArray[i][j] = (int) (Math.random() * 100);
	        }
	    }

	    System.out.println("\nSource array");
	    for (int i = 0; i < twoDimensionalArray.length; i++){
	        for (int j = 0; j < twoDimensionalArray[i].length; j++){
	            System.out.print(twoDimensionalArray[i][j] + " ");
	        }
	        System.out.println("");
	    }
	    
	    // transposing	    
	    for (int i = 0; i < twoDimensionalArray.length; i++) {
	    	for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	    		transposedArray[i][j] = twoDimensionalArray[j][i];
	    	}
	    }
	    
		long startTime = System.currentTimeMillis();
	    
	    // sorting
	    for (int i = 0; i < transposedArray.length; i++) {
	    	Arrays.sort(transposedArray[i]);
	    }
	    
	    long estimatedTime = System.currentTimeMillis() - startTime;
	    
	    // transposing back
	    for (int i = 0; i < transposedArray.length; i++) {
	    	for (int j = 0; j < transposedArray[i].length; j++) {
	    		twoDimensionalArray[i][j] = transposedArray[j][i];
	    	}
	    }
        
	    System.out.println("\nModified array");
	    for (int i = 0; i < twoDimensionalArray.length; i++){
	        for (int j = 0; j < twoDimensionalArray[i].length; j++){
	            System.out.print(twoDimensionalArray[i][j] + " ");
	        }
	        System.out.println("");
	    }
	    
		System.out.println("\nSorting time: " + estimatedTime);
	}	
}
