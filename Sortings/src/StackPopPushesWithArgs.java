import java.util.Stack;

public class StackPopPushesWithArgs {

	public void Run(String[] args) {
		
		Boolean useDefault = args.length != 2;
		
		int queueLength = useDefault ? 10 : Integer.parseInt(args[0]);
		int stackCount = useDefault ? 3 : Integer.parseInt(args[1]);		
		
		Stack<Integer>[] stacks = (Stack<Integer>[]) new Stack[stackCount];
		Stack<Integer> stack = new Stack<Integer>();
		
		for (int i = 0; i < stackCount; i++) {
			stacks[i] = new Stack<Integer>();
		}
		
		int[] popPushQueue = new int[queueLength];
		
        for (int i = 0; i < popPushQueue.length; i++) {
        	popPushQueue[i] = (int) (Math.random() * 2);
	    }
        
        ShowArrayWithMessage(popPushQueue, "\nPopPushQueue: ");
        
        System.out.println("\n");
        
        int poppedValue = 0;
        int pushedValue = 0;
        int popCounter = 0;
        int pushCounter = 0;
        int[] popArray = new int[queueLength];
        int[] pushArray = new int[queueLength];
        
        for (int i = 0; i < popPushQueue.length; i++) {
        	
        	int randomStackNumber = (int) (Math.random() * stackCount);
        	Stack<Integer> randomStack = stacks[randomStackNumber];
        	
        	if (popPushQueue[i] == 1) {        		
        		pushedValue = (int) (Math.random() * 100);
        		randomStack.push(pushedValue);
        		pushArray[pushCounter] = pushedValue;        		
        		pushCounter++;
        		
        		System.out.println("pushed to stack #" + (randomStackNumber + 1) + ": " + pushedValue);
        	} else {      		
        		if (!randomStack.isEmpty()) {
        			poppedValue = randomStack.pop();
        			popArray[popCounter] = poppedValue;
        			popCounter++;
        			
        			System.out.println("popped from stack #" + (randomStackNumber + 1) + ": " + poppedValue);
        		} else {
        			System.out.println("can't pop from stack #" + (randomStackNumber + 1) + ", stack is empty");
        		}
        	}
        }
        
        ShowArrayWithMessage(pushArray, "\npushed values: ");
        ShowArrayWithMessage(popArray, "\npopped values: ");
	}
	
	private void ShowArrayWithMessage(int[] array, String message) {
		
	    System.out.println(message);
	    for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
	    }		
	}		
}
