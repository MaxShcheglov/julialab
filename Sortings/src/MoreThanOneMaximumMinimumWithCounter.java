
public class MoreThanOneMaximumMinimumWithCounter {

	public void Run() {
		
	    int maxRows = 3;
	    int maxCols = 4;
	    int maxCounter = 0;
	    int minCounter = 0;

	    int [][] twoDimensionalArray = new int[maxRows][];
        for (int i = 0; i < maxRows; i++){
        	twoDimensionalArray[i] = new int[maxCols];
	    }

        for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	            twoDimensionalArray[i][j] = (int) (Math.random() * 100);
	        }
	    }

	    System.out.println("\nSource array");
	    for (int i = 0; i < twoDimensionalArray.length; i++){
	        for (int j = 0; j < twoDimensionalArray[i].length; j++){
	            System.out.print(twoDimensionalArray[i][j] + " ");
	        }
	        System.out.println("");
	    }
	    
	    int maxValue = 1;
	    for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	        	if (twoDimensionalArray[i][j] > maxValue) {
	        		maxValue = twoDimensionalArray[i][j];
	        	}
	            
	        }
        }
	    
	    int minValue = 100;
	    for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	        	if (twoDimensionalArray[i][j] < minValue) {
	        		minValue = twoDimensionalArray[i][j];
	        	}
	            
	        }
	    }	 
	    
	    System.out.println("\nMax values in array: ");
	    for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	        	if (twoDimensionalArray[i][j] == maxValue) {
	        		System.out.println("array [" + i + "][" + j + "]: " + maxValue);
	        		maxCounter++;
	        	}	            
	        }
        }
	    System.out.println("\nnumber of max values: " + maxCounter);
	    
	    System.out.println("\nMin values in array: ");
	    for (int i = 0; i < twoDimensionalArray.length; i++) {
	        for (int j = 0; j < twoDimensionalArray[i].length; j++) {
	        	if (twoDimensionalArray[i][j] == minValue) {
	        		System.out.println("array [" + i + "][" + j + "]: " + minValue);
	        		minCounter++;
	        	}
	        }
        }
	    
	    System.out.println("\nnumber of min values: " + minCounter);
	}
	
}
