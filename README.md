# Issues
    + refactor, one project for all tasks, proper modifier specificators, main in classes
    - browser emulation with two stacks and array, complexity comparison
    + toString method overriding
    + polymorphic method calling, impossible to determine what method will call (shapes example)
    + int array queue
    + system queue demonstration
    + stack super class and queue (with int array and object array) implementation using inheritance
    + stack and queue demonstration with cmd line args, random actions sequence, user input
    + int and big int fractions sorting with your interface and IComparable
    + implement function that can sum array of int fractures
    + implement function that can sum array of big int fractures
    + there is list of students group with last name and average score sorted by last name.
      return new document where students sorted by average score, create program