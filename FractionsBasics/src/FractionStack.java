public class FractionStack {
    private Fraction[] array;
    private int index;

    public FractionStack() {
        array = new Fraction[10];
        index = -1;
    }

    public void push(Fraction element) {
        array[++index] = element;
    }

    public Fraction peek() {
        return array[index];
    }

    public Fraction pop() {
        return array[index--];
    }
}