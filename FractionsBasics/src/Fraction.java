public class Fraction implements Comparable<Fraction> {
    private int numerator;
    private int denominator;

    public Fraction() {
        numerator = 0;
        denominator = 1;
    }

    public Fraction(int wholeNumber) {
        numerator = wholeNumber;
        denominator = 1;
    }

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    // конструктор для строки
    public Fraction(String fraction) {
        String[] numbers = fraction.split("/");
        this.numerator = Integer.parseInt(numbers[0]);
        this.denominator = Integer.parseInt(numbers[1]);
    }

    public Fraction add(Fraction addend) {
        Fraction result = new Fraction();
        int commonDenominator = denominator * addend.denominator;
        result.denominator = commonDenominator;
        result.numerator = numerator * addend.denominator + addend.numerator * denominator;
        return result;
    }

    public Fraction substract(Fraction subtrahend) {
        Fraction result = new Fraction();
        int commonDenominator = denominator * subtrahend.denominator;
        result.denominator = commonDenominator;
        result.numerator = numerator * subtrahend.denominator - subtrahend.numerator * denominator;
        return result;
    }

    public Fraction multiply(Fraction multiplier) {
        Fraction result = new Fraction();
        result.numerator = numerator * multiplier.numerator;
        result.denominator = denominator * multiplier.denominator;
        return result;
    }

    public Fraction divide(Fraction divider) {
        Fraction result = new Fraction();
        result.numerator = numerator * divider.denominator;
        result.denominator = denominator * divider.numerator;
        return result;
    }

    public String toString() {
        return numerator + "/" + denominator;
    }

    public boolean equals(Fraction fraction) {
        return fraction.numerator == this.numerator && fraction.denominator == this.denominator;
    }

    public boolean isNegative() {
        return numerator < 0 || denominator < 0;
    }

    public int compare(Fraction fraction) {
        if (this.equals(fraction)) {
            return 0;
        } else if (this.substract(fraction).isNegative()) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public int compareTo(Fraction o) {
        if (this.equals(o)) {
            return 0;
        } else if (this.substract(o).isNegative()) {
            return -1;
        } else {
            return 1;
        }
    }
}

