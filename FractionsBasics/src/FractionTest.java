import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FractionTest {

    @Test
    void add() {
        Fraction fraction1 = new Fraction(1,2);
        Fraction fraction2 = new Fraction(1,3);

        Fraction actual = fraction1.add(fraction2);

        Fraction expected = new Fraction(5,6);
        assertTrue(expected.equals(actual));
    }

    @Test
    void substract() {
        Fraction fraction1 = new Fraction(2,3);
        Fraction fraction2 = new Fraction(1,4);

        Fraction actual = fraction1.substract(fraction2);

        Fraction expected = new Fraction(5,12);
        assertTrue(expected.equals(actual));
    }

    @Test
    void multiply() {
        Fraction fraction1 = new Fraction(1,2);
        Fraction fraction2 = new Fraction(1,2);

        Fraction actual = fraction1.multiply(fraction2);

        Fraction expected = new Fraction(1,4);
        assertTrue(expected.equals(actual));
    }

    @Test
    void divide() {
        Fraction fraction1 = new Fraction(1,2);
        Fraction fraction2 = new Fraction(1,3);

        Fraction actual = fraction1.divide(fraction2);

        Fraction expected = new Fraction(3,2);
        assertTrue(expected.equals(actual));
    }
}