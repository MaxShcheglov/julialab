import java.math.BigInteger;

public class BigIntegerFraction implements Comparable<BigIntegerFraction> {
    private BigInteger numerator;
    private BigInteger denominator;

    public BigIntegerFraction() {
        numerator = BigInteger.valueOf(0);
        denominator = BigInteger.valueOf(1);
    }

    public BigIntegerFraction(long wholeNumber) {
        numerator = BigInteger.valueOf(wholeNumber);
        denominator = BigInteger.valueOf(1);
    }

    public BigIntegerFraction(long numerator, long denominator) {
        this.numerator = BigInteger.valueOf(numerator);
        this.denominator = BigInteger.valueOf(denominator);
    }

    public BigIntegerFraction(String fraction) {
        String[] numbers = fraction.split("/");
        this.numerator = BigInteger.valueOf(Long.parseLong(numbers[0]));
        this.denominator = BigInteger.valueOf(Long.parseLong(numbers[1]));
    }

    public BigIntegerFraction add(BigIntegerFraction addend) {
        BigIntegerFraction result = new BigIntegerFraction();
        BigInteger commonDenominator = denominator.multiply(addend.denominator);
        result.denominator = commonDenominator;
        result.numerator = numerator.multiply(addend.denominator).add(denominator.multiply(addend.numerator));
        return result;
    }

    public BigIntegerFraction substract(BigIntegerFraction subtrahend) {
        BigIntegerFraction result = new BigIntegerFraction();
        BigInteger commonDenominator = denominator.multiply(subtrahend.denominator);
        result.denominator = commonDenominator;
        result.numerator = numerator.multiply(subtrahend.denominator).subtract(subtrahend.numerator.multiply(denominator));
        return result;
    }

    public BigIntegerFraction multiply(BigIntegerFraction multiplier) {
        BigIntegerFraction result = new BigIntegerFraction();
        result.numerator = numerator.multiply(multiplier.numerator);
        result.denominator = denominator.multiply(multiplier.denominator);
        return result;
    }

    public BigIntegerFraction divide(BigIntegerFraction divider) {
        BigIntegerFraction result = new BigIntegerFraction();
        result.numerator = numerator.multiply(divider.denominator);
        result.denominator = denominator.multiply(divider.numerator);
        return result;
    }

    public String toString() {
        return numerator + "/" + denominator;
    }

    public boolean equals(BigIntegerFraction fraction) {
        return fraction.numerator == this.numerator && fraction.denominator == this.denominator;
    }

    public boolean isNegative() {
        return numerator.compareTo(BigInteger.ZERO) < 0 || denominator.compareTo(BigInteger.ZERO) < 0;
    }

    public int compare(BigIntegerFraction fraction) {
        if (this.equals(fraction)) {
            return 0;
        } else if (this.substract(fraction).isNegative()) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public int compareTo(BigIntegerFraction o) {
        if (this.equals(o)) {
            return 0;
        } else if (this.substract(o).isNegative()) {
            return -1;
        } else {
            return 1;
        }
    }
}
