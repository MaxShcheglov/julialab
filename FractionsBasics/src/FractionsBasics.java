import java.security.InvalidParameterException;
import java.util.Arrays;

public class FractionsBasics {

    public static void main(String[] args)
    {
        FractionsDemonstration();
        BigIntegerFractionsDemonstrations();
    }

    private static void FractionsDemonstration() {

        Fraction[] array = new Fraction[5];

        array[0] = new Fraction(1, 2);
        array[1] = new Fraction(1,3);
        array[2] = new Fraction(2,3);
        array[3] = new Fraction(5,4);
        array[4] = new Fraction("6/7");

        showArrayWithMessage(array, "source array: ");

        System.out.println("minimal fraction: " + findMinimalFraction(array));
        System.out.println("maximal fraction: " + findMaximalFraction(array));
        System.out.println("multiply of fractions: " + calculateMultiplyOfFractions(array));
        System.out.println("sum of fractions: " + calculateSumOfFractions(array));

        Fraction fraction1 = new Fraction(1,2);
        Fraction fraction2 = new Fraction(2,3);

        System.out.println("\naddition: " + fraction1 + " + " + fraction2 + " = " + fraction1.add(fraction2));
        System.out.println("subtraction: " + fraction1 + " - " + fraction2 + " = " + fraction1.substract(fraction2));
        System.out.println("multiplication: " + fraction1 + " * " + fraction2 + " = " + fraction1.multiply(fraction2));
        System.out.println("division: " + fraction1 + " : " + fraction2 + " = " + fraction1.divide(fraction2));

        FractionStack stack = new FractionStack();
        stack.push(new Fraction("1/2"));
        stack.push(new Fraction("6/9"));
        stack.push(new Fraction("8/3"));
        System.out.println("\npopped element: " + stack.pop());
        System.out.println("peeked element: " + stack.peek());
        System.out.println("popped element: " + stack.pop());

        sortArrayWithBubbleSorting(array);
        showArrayWithMessage(array, "\narray with bubble sorting: ");
        sortArrayWithInsertSorting(array);
        showArrayWithMessage(array, "array with insert sorting: ");
        sortArrayWithUsingComparableInterface(array);
        showArrayWithMessage(array, "array sorted using comparable interface: ");

        String expression = "1/2+2/3+5/6";
        System.out.println("\ncalculate expression: " + expression + " = " + calculateStringExpression(expression));
        expression = "2/3-5/6";
        System.out.println("calculate expression: " + expression + " = " + calculateStringExpression(expression));
        expression = "6/7*1/3*7/3";
        System.out.println("calculate expression: " + expression + " = " + calculateStringExpression(expression));
        expression = "5/3:2/6";
        System.out.println("calculate expression: " + expression + " = " + calculateStringExpression(expression));
    }

    private static void BigIntegerFractionsDemonstrations() {

        BigIntegerFraction[] array = new BigIntegerFraction[5];

        array[0] = new BigIntegerFraction(14252, 224524);
        array[1] = new BigIntegerFraction(1345345,245345);
        array[2] = new BigIntegerFraction(134534,2252525);
        array[3] = new BigIntegerFraction(145345,23525);
        array[4] = new BigIntegerFraction("1/2");

        showArrayWithMessage(array, "\nsource big integer array: ");

        sortArrayWithUsingComparableInterface(array);
        showArrayWithMessage(array, "array sorted using comparable interface: ");

        System.out.println("sum of array with BigInteger fractions: " + calculateSumOfBigIntegerFractions(array));
    }

    private static void showArrayWithMessage(Object[] array, String message) {
        System.out.print(message);
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
    }

    private static Fraction findMaximalFraction(Fraction[] array) {
        Fraction maximalFraction = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                if (array[i].compare(maximalFraction) == 1) {
                    maximalFraction = array[i];
                }
            }
        }
        return maximalFraction;
    }

    private static Fraction findMinimalFraction(Fraction[] array) {
        Fraction minimalFraction = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                if (array[i].compare(minimalFraction) == -1) {
                    minimalFraction = array[i];
                }
            }
        }
        return minimalFraction;
    }

    private static Fraction calculateMultiplyOfFractions(Fraction[] array) {
        Fraction result = new Fraction(1, 1);
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                result = result.multiply(array[i]);
            }
        }
        return result;
    }

    private static Fraction calculateSumOfFractions(Fraction[] array) {
        Fraction result = new Fraction();
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                result = result.add(array[i]);
            }
        }
        return result;
    }

    private static BigIntegerFraction calculateSumOfBigIntegerFractions(BigIntegerFraction[] array) {
        BigIntegerFraction result = new BigIntegerFraction();
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                result = result.add(array[i]);
            }
        }
        return result;
    }

    private static Fraction calculateSubstracionOfFractions(Fraction[] array) {
        Fraction result = new Fraction();
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                result = result.substract(array[i]);
            }
        }
        return result;
    }

    private static Fraction calculateDivisionOfFractions(Fraction[] array) {
        Fraction result = new Fraction(1, 1);
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                result = result.divide(array[i]);
            }
        }
        return result;
    }

    private static void sortArrayWithBubbleSorting(Fraction[] array) {
        int numberOfElements = getNumberOfFractions(array);
        Fraction temp = new Fraction();
        for (int i = 0; i < numberOfElements; i++){
            for (int j = 1; j < numberOfElements - i; j++){
                if (array[j - 1].compare(array[j]) == 1){
                    //swap elements
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    private static void sortArrayWithUsingComparableInterface(Object[] array) {
        Arrays.sort(array);
    }

    private static int getNumberOfFractions(Fraction[] array) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                counter++;
            }
        }
        return counter;
    }

    private static void sortArrayWithInsertSorting(Fraction[] array) {
        int numberOfElements = getNumberOfFractions(array);
        Fraction temp = new Fraction();
        int j = 0;
        for (int i = 0; i < numberOfElements - 1; i++){
            if (array[i].compare(array[i + 1]) == 1) {
                temp = array[i + 1];
                array[i + 1] = array[i];
                j = i;
                while (j > 0 && temp.compare(array[j - 1]) == -1) {
                    array[j] = array[j - 1];
                    j--;
                }
                array[j] = temp;
            }
        }
    }

    private static Fraction calculateStringExpression(String expression) {
        if (expression.contains("+")) {
            String[] operands = expression.split("\\+");
            return calculateSumOfFractions(convertToArrayOfFractions(operands));
        } else if (expression.contains("-")) {
            String[] operands = expression.split("\\-");
            return calculateSubstracionOfFractions(convertToArrayOfFractions(operands));
        } else if (expression.contains("*")) {
            String[] operands = expression.split("\\*");
            return calculateMultiplyOfFractions(convertToArrayOfFractions(operands));
        } else if (expression.contains(":")) {
            String[] operands = expression.split("\\:");
            return calculateDivisionOfFractions(convertToArrayOfFractions(operands));
        }

        throw new InvalidParameterException("cant calculate");
    }

    private static Fraction[] convertToArrayOfFractions(String[] array) {
        Fraction[] fractions = new Fraction[array.length];
        for (int i = 0; i < array.length; i++) {
            fractions[i] = new Fraction(array[i]);
        }
        return fractions;
    }
}