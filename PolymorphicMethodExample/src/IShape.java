public interface IShape {
    double area();
    double perimeter();
}
