import java.util.ArrayList;

public class PolymorphicMethod {
    public static void main(String[] args) {

        Circle circle = new Circle(3);
        Square square = new Square(5);
        Rectangle rectangle = new Rectangle(2, 4);

        ArrayList<IShape> shapes = new ArrayList<IShape>();
        shapes.add(circle);
        shapes.add(square);
        shapes.add(rectangle);

        System.out.println("there are some shapes:\n");
        for (IShape shape : shapes) {
            System.out.println(shape);
            System.out.println("\tarea: " + shape.area());
            System.out.println("\tperimeter: " + shape.perimeter());
            System.out.println();
        }
    }
}

